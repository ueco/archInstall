## Arch Linux

#### Misc Tweaks

###### Disable turbo boost:
  `# echo 1 > /sys/devices/system/cpu/intel_pstate/no_turbo`

###### Disable systemd from handling lid close events:
Edit `/etc/systemd/logind.conf` and set `HandleLidSwitch` to `ignore`.

###### Installation procedure (encrypted lvm on EFI):
  1. Use wifi-menu to connect to network
  2. Create efi partition:

       `# fdisk /dev/sda`

          * g (to create an empty GPT partition table)
          * n
          * 1
          * enter
          * +300M
          * t
          * 1 (For EFI)
          * w

  3. Create boot partition:

      `# fdisk /dev/sda`

         * n
         * 2
         * enter
         * +400M
         * w

  4. Create LVM partition:

       `# fdisk /dev/sda`

          * n
          * 3
          * enter
          * enter
          * t
          * 3
          * 30
          * w

  5. `# mkfs.fat -F32 /dev/sda1`
  6. `# mkfs.ext2 /dev/sda2`
  7. Set up encryption
        * `# cryptsetup -v --type luks --cipher aes-xts-plain64 --key-size 512 --hash sha512 --iter-time 5000 --use-random --verify-passphrase luksFormat /dev/sda3`
        * `# cryptsetup open --type luks /dev/sda3 lvm`
  8. Set up lvm:
        * `# pvcreate --dataalignment 1m /dev/mapper/lvm`
        * `# vgcreate volgroup0 /dev/mapper/lvm`
        * `# lvcreate -L 250GB volgroup0 -n root`
        * `# modprobe dm_mod`
        * `# vgscan`
        * `# vgchange -ay`
  9. `# mkfs.ext4 /dev/volgroup0/lv_root`
  10. `# mount /dev/volgroup0/lv_root /mnt`
  11. `# mkdir /mnt/boot`
  12. `# mount /dev/sda2 /mnt/boot`
  13. `# mkdir /mnt/hostrun`
  14. `# mount --bind /run /mnt/hostrun`
  15. `# pacstrap /mnt base base-devel linux-firmware i3-wm i3status dmenu lvm2 i3lock networkmanager network-manager-applet xorg xorg-server xorg-xinit grub efibootmgr dosfstools openssh os-prober mtools linux-headers linux-lts linux-lts-headers zsh clang wget vim vlc firefox termite thunderbird git zip unzip`
  16. `# genfstab -U -p /mnt >> /mnt/etc/fstab`
  17. `# arch-chroot /mnt`
  18. `# mkdir /run/lvm`
  19. `# mount --bind /hostrun/lvm /run/lvm`
  20. Edit `/etc/mkinitcpio.conf` and add `ext4 xfs` to MODULES and `encrypt lvm2` in between `block` and `filesystems` to HOOKS.
  21. `# mkinitcpio -p linux`
  22. `# mkinitcpio -p linux-lts`
  23. `# vim /etc/locale.gen` (uncomment en_US.UTF-8, pl_PL.UTF-8)
  24. `# locale-gen`
  25. `# echo "LANG=en_US.UTF-8" > /etc/locale.conf`
  26. `# echo "KEYMAP=pl" > /etc/vconsole.conf`
  27. `# echo "*myhostname*" > /etc/hostname`
  28. Add matching entries to `/etc/hosts`:

```
127.0.0.1	localhost
::1		localhost
127.0.1.1	*myhostname*.localdomain *myhostname*
```

  29. `# ln -sf /usr/share/zoneinfo/Europe/Berlin /etc/localtime`
  30. `# passwd` (for setting root password)
  31. `# useradd -g wheel -m -s /bin/zsh username`
  32. `# passwd username`
  33. Edit `/etc/default/grub`:
        add `cryptdevice=<PARTUUID>:volgroup0` to the `GRUB_CMDLINE_LINUX_DEFAULT` line<br>
            If using standard device naming, the option will look like this: `cryptdevice=/dev/sda3:volgroup0`
  34. `# mkdir /boot/EFI`
  35. `# mount /dev/sda1 /boot/EFI`
  36. `# grub-install --target=x86_64-efi  --bootloader-id=grub_uefi --recheck`
  37. `# cp /usr/share/locale/en\@quot/LC_MESSAGES/grub.mo /boot/grub/locale/en.mo`
  38. `# grub-mkconfig -o /boot/grub/grub.cfg`
  39. `# umount -a`

###### After installation:
  1. `cp /etc/X11/xinit/xinitrc ~/.xinitrc`
  2. Add on bottom of `.xinitrc` file `exec i3`
  3. Add to `~/.zprofile` and/or `~/.bash_profile`:

```
if [[ ! $DISPLAY && $XDG_VTNR -eq 1 ]]; then
    exec startx
fi
```

  4. Add to `~/.config/user-dirs.dirs`:

```
XDG_DESKTOP_DIR="$HOME/downloads"
XDG_DOCUMENTS_DIR="$HOME/documents"
XDG_DOWNLOAD_DIR="$HOME/downloads"
XDG_MUSIC_DIR="$HOME/downloads"
XDG_PICTURES_DIR="$HOME/images"
XDG_PUBLICSHARE_DIR="$HOME/downloads"
XDG_TEMPLATES_DIR="$HOME/downloads"
XDG_VIDEOS_DIR="$HOME/downloads"
```

  5. `sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"`
  6. `git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions`
